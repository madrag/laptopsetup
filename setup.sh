#!/usr/bin/env bash
set -eu -o pipefail

printf "Installing git and ansible...\n"
sudo apt install -y ansible

printf "Laptop setup...\n"
ansible-playbook main.yml

printf "Setup done...\n Change your password!!!\n"